#!/bin/bash

# Setting passwords before set -e and -x so as not to make them appear in logs
adminPassword="$(openssl rand -base64 12)aL1"
databasePassword="$(openssl rand -base64 12)aL1"

set -e
set -x

cd "$(dirname "$0")"

#Generic setup

builddef='./build.json'
releasedef='./release.json'

if  [[ -z $APPLICATION_INSTANCE_NAME ]] ; then
	PROJECT_NAME=$1
	timestamp="$(date +"%s")"
	APPLICATION_INSTANCE_NAME=nweb$timestamp
fi

#var checks
: "${PROJECT_NAME?Need to set PROJECT_NAME}"
: "${VSTS_AZURE_SERVICE?Need to set VSTS_AZURE_SERVICE}"
: "${VSTS_AZURE_DOCKER_REGISTRY?Need to set VSTS_AZURE_DOCKER_REGISTRY}"
: "${VSTS_AZURE_DOCKER_REG_PSWD?Need to set VSTS_AZURE_DOCKER_REG_PSWD}"

#Product specific vars
PRODUCT_NAME=nodejs-webapp
releasedefname=${PRODUCT_NAME}_${APPLICATION_INSTANCE_NAME}

AZURE_SERVICE_ID=$(vsts-getservices $PROJECT_NAME | jq -r  ".[] | select(.name==\"$VSTS_AZURE_SERVICE\") | .id")
VSTS_AZURE_DOCKER_REGISTRY_LOWERCASE=$(echo "$VSTS_AZURE_DOCKER_REGISTRY" | tr '[:upper:]' '[:lower:]')
VSTS_DOCKER_REGISTRY_HOST=$VSTS_AZURE_DOCKER_REGISTRY_LOWERCASE.azurecr.io
REGISTRY_INPUT_STRING="{\"loginServer\":\"$VSTS_DOCKER_REGISTRY_HOST\"}"
PROJECT_NAME_LOWERCASE=$(echo "$PROJECT_NAME" | tr '[:upper:]' '[:lower:]')

#Go
vsts-import $PROJECT_NAME \
-g https://bitbucket.org/automationlogic/demoapplication \
	-b $builddef \
	-r $releasedef \
	--releaseagent 'Default' \
	--buildagent 'Hosted Linux Preview' \
    --releaseservice "connectedServiceNameARM=$VSTS_AZURE_SERVICE" \
	--releaseservice "ConnectedServiceName=$VSTS_AZURE_SERVICE" \
	--releaseservice "azureSubscriptionEndpoint=$VSTS_AZURE_SERVICE" \
	--buildservice "azureSubscriptionEndpoint=$VSTS_AZURE_SERVICE" \
	-v applicationName=$APPLICATION_INSTANCE_NAME \
	-v Deploy:adminPassword=$adminPassword \
	-v networkIpAddress="\"name\":\"ip01\",\"resourceGroup\":\"$APPLICATION_INSTANCE_NAME\",\"domainNameLabel\":\"$APPLICATION_INSTANCE_NAME\",\"publicIPAllocationMethod\":\"static\",\"newOrExistingOrNone\":\"new\"" \
	-v databaseName=${APPLICATION_INSTANCE_NAME}db \
	-v databaseAdminPassword=$databasePassword \
	-v containerimage=$VSTS_DOCKER_REGISTRY_HOST/${PROJECT_NAME_LOWERCASE}:latest \
	-v ircServer=$VSTS_DOCKER_REGISTRY_HOST \
	-v ircUsername=$VSTS_AZURE_DOCKER_REGISTRY \
	-v ircPassword=$VSTS_AZURE_DOCKER_REG_PSWD \
	-v dockerRegistryUrl=https://$VSTS_DOCKER_REGISTRY_HOST \
	-v webappname=${APPLICATION_INSTANCE_NAME}webapp \
	-v resourceGroup=$APPLICATION_INSTANCE_NAME \
	-v vaultName=${APPLICATION_INSTANCE_NAME}vault \
	-x "$.process.phases[*].steps[?(@.displayName=='Build an image')].inputs.azureContainerRegistry=$REGISTRY_INPUT_STRING" \
	-x "$.process.phases[*].steps[?(@.displayName=='Push an image')].inputs.azureContainerRegistry=$REGISTRY_INPUT_STRING" \
	--releasedefname $releasedefname \
	--no-git \
	--no-autobuild
# remove --no-autobuild for build definition triggered automatically
