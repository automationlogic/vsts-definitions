#Node App on Azure Web App for Containers

Read the [import instructions](https://bitbucket.org/automationlogic/vsts-rest/src) first.

##Prerequisites:

Install the client and update vsts-import

```
# Update npm 
npm i npm 

# Install the client and update version
npm i -g vsts-rest

```


Create an Azure Container Registry resource

```
az group create --name <ResourceGroupName> --location <location>
az acr create --name <ContainerRegistryName> \
              --resource-group <ResourceGroupName> \
              --sku Standard \
              --admin-enabled true

```

Grab the Azure Container Registry password <ContainerRegistryPassword>

```
az acr credential show --name <ContainerRegistryName> --resource-group <ResourceGroupName> --query passwords[0].value

```

Export the following variables

```
export VSTS_PAT
export VSTS_ACCOUNT
export VSTS_USER
export VSTS_AZURE_SERVICE
export VSTS_SONARQUBE_SERVICE
export VSTS_AZURE_DOCKER_REGISTRY=<ContainerRegistryName>
export VSTS_AZURE_DOCKER_REG_PSWD=<ContainerRegistryPassword>

```

##Deploy

To deploy, run

```
importDef.sh <ProjectName> <ResourceGroupName>

```


The resource group must already exist. We have only tested westeurope thus far.

More docs [here](https://automationlogic.atlassian.net/wiki/spaces/ASC/pages)
