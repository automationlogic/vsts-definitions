#!/bin/bash

# Setting passwords before set -e and -x so as not to make them appear in logs
adminPassword="$(openssl rand -base64 12)aL1"
databasePassword="$(openssl rand -base64 12)aL1"

set -e
set -x

cd "$(dirname "$0")"

#Generic setup

builddef='./build.json'
releasedef='./release.json'

if  [[ -z $APPLICATION_INSTANCE_NAME ]] ; then
	PROJECT_NAME=$1
	timestamp="$(date +"%s")"
	APPLICATION_INSTANCE_NAME=nvmss$timestamp
fi

#Product specific vars
PRODUCT_NAME=nodejs-vmscaleset
releasedefname=${PRODUCT_NAME}_${APPLICATION_INSTANCE_NAME}

#var checks
: "${PROJECT_NAME?Need to set PROJECT_NAME}"
: "${VSTS_AZURE_SERVICE?Need to set VSTS_AZURE_SERVICE}"

#Go
vsts-import $PROJECT_NAME \
-g https://bitbucket.org/automationlogic/demoapplication \
	-b ./build.json \
	-r ./release.json \
	--releaseagent 'Default' \
	--buildagent 'Hosted Linux Preview' \
    --releaseservice "connectedServiceNameARM=$VSTS_AZURE_SERVICE" \
	--releaseservice "ConnectedServiceName=$VSTS_AZURE_SERVICE" \
	--releaseservice "azureSubscriptionEndpoint=$VSTS_AZURE_SERVICE" \
	-v applicationName=${APPLICATION_INSTANCE_NAME} \
	-v Deploy:adminPassword=$adminPassword \
    -v Deploy:databaseAdminPassword=$databasePassword \
	-v networkIpAddress="\"name\":\"ip01\",\"resourceGroup\":\"${APPLICATION_INSTANCE_NAME}\",\"domainNameLabel\":\"${APPLICATION_INSTANCE_NAME}\",\"publicIPAllocationMethod\":\"static\",\"newOrExistingOrNone\":\"new\"" \
	-v databaseName=${APPLICATION_INSTANCE_NAME}db \
	-v vaultName=${APPLICATION_INSTANCE_NAME}vault \
	-v resourceGroup=$APPLICATION_INSTANCE_NAME \
	--releasedefname $releasedefname \
	--no-git \
	--no-autobuild
