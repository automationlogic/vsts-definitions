#Node App on Azure VM Scaleset

Read the [import instructions](https://bitbucket.org/automationlogic/vsts-rest/src) first.

To deploy, run 

```
importDef.sh <ProjectName> <ResourceGroupName>

```


The resource group must already exist. We have only tested westeurope thus far.

More docs [here](https://automationlogic.atlassian.net/wiki/spaces/ASC/pages)