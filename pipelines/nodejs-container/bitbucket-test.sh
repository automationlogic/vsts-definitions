#!/bin/bash

set -e
set -x

: "${PROJECT_NAME?Need to set PROJECT_NAME}"

PRODUCT_NAME=nodejs-container
timestamp="$(date +"%s")"
APPLICATION_INSTANCE_NAME=nc$timestamp
location=westeurope
releasedefname=${PRODUCT_NAME}_${APPLICATION_INSTANCE_NAME}
export endpoint=${APPLICATION_INSTANCE_NAME}container.${location}.azurecontainer.io:3000
export sleep=5

# Destroy will inititiate 5 mins after successful completion or failure.

function cleanup {
  case "$OSTYPE" in
    darwin*)  echo "OSX" && destroytime="$(date -v +5M)";;
    linux*)   echo "LINUX" && destroytime="$(date --date='5min')";;
    *)        echo "unknown: $OSTYPE" ;;
  esac
  echo Destroy will initiate at $destroytime
  sleep 300
  vsts-release $PROJECT_NAME --releaseid $RELEASE_ID --releasedef $releasedefname --artifact $PRODUCT_NAME -e Destroy -m Update -E Destroy --wait
}
trap '[ "$?" -eq 0 ] || cleanup' ERR EXIT

function endpointtest {
  echo endpointTest
  bash <(curl -s https://bitbucket.org/automationlogic/demoapplication/raw/3f8955abe556ac4ee437f7f812339d7fa7ec7e07/tests/endpointTest.sh)
}

cd ./pipelines/$PRODUCT_NAME/

APPLICATION_INSTANCE_NAME=$APPLICATION_INSTANCE_NAME ./importDef.sh

vsts-build $PROJECT_NAME --build $PRODUCT_NAME --wait

echo creating resource group $APPLICATION_INSTANCE_NAME and deploying
vsts-release $PROJECT_NAME --releasedef $releasedefname --artifact $PRODUCT_NAME -e Deploy -m Update -E Deploy --wait | tee deploy-$PRODUCT_NAME.log

RELEASE_ID=`cat deploy-$PRODUCT_NAME.log | yq '.["release-id"]'`

endpointtest

echo updating app
vsts-release $PROJECT_NAME --releaseid $RELEASE_ID --releasedef $releasedefname --artifact $PRODUCT_NAME -e Update -m Deploy -E Update --wait

endpointtest

cleanup
