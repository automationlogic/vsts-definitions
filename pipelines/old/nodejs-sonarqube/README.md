# Sonarqube with Node App on Azure VM Scaleset

Read the [import instructions](https://bitbucket.org/automationlogic/vsts-rest/src) first.

To deploy, run

```
sonarImportDef.sh <ProjectName> <ResourceGroupName>

```

The resource group must already exist. We have only tested westeurope thus far.

## Sonarqube specific configuration
Steps required

- Have available a Sonarqube server with public IP address
- Create A Project on Sonarqube server and retrieve project settings
- Create a 'Sonarqube Service Endpoint'
- Install Sonarqube market place extension
- Adjust vars on Sonarqube build step to specifics from above
- Ensure Service Endpoint is referenced in build step
- Run build

Specifics

- Have available a Sonarqube server with public IP address
	- Connect to server via web page to configure Project
- Create A Project on Sonarqube server and retrieve project settings
	- Settings required from Project
		- URL and port of project
		- project name
		- project key
		- token
- Create a 'Sonarqube Service Endpoint'
	- Under services tab create a 'New Service Endpoint' type = SonarQube
		- Give 'Connection' a name (this will be required as a var for build step)
		- Supply serverURL:port from above steps
		- Supply token from above steps
- Install Sonarqube market place extension
	- Under build steps, add sonarqube as a step and follow instructions
- Adjust vars on Sonarqube build stage to specifics from above
	- projectKey    = (from above)
	- ProjectName   = (from above)
- Ensure Service Endpoint is referenced in build step
	- Check that the Service Endpoint is pointing to the one you created
- Run build

More docs [here](https://automationlogic.atlassian.net/wiki/spaces/ASC/pages)
