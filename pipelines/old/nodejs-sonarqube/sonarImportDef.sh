# example;
# bash importDef.sh newProj2 nodejsvmrrc
vsts-import $1 \
-g https://bitbucket.org/automationlogic/demoapplication \
	-b ./buildProcess.json \
	-r ./releaseProcess-deploy.json \
	--releaseagent 'LinuxDefault' \
	-v adminUsername=aladminuser \
	-v applicationName=nodejsvmsclapp \
	-v osPlatform=Linux \
	-v instanceCount=1 \
	-v databaseSelection=MySQL \
	-v osVersion='"publisher":"Canonical","offer":"UbuntuServer","sku":"16.04.0-LTS","version":"latest"' \
	-v instanceSize=Standard_A1 \
	-v framework=nodejs \
	-v adminUsername=aladminuser \
	-v adminPassword='TestP$SSw0r1.3!' \
	-v instanceSize=Standard_A1 \
	-v DEV:networkIpAddress='"name":"ip01","resourceGroup":"jktestparam1","domainNameLabel":"alnodejsdevapp","publicIPAllocationMethod":"static","newOrExistingOrNone":"new"' \
	-v DEV:databaseName=almysqldevdb01 \
	-v databaseAdminPassword='TestP$SSw0r1.3!' \
	-v databaseAdminUsername=ALAdminDBUser \
	-v applicationPort=3000 \
	-v location=westeurope \
	-v DEV:resourceGroup=$2 \
	-v DEV:applicationEnvironment=dev \
	-v projectName=npm-todo-project-key \
	-v projectKey=npm-todo-project-key \
