timestamp="$(date +"%s")"

: "${VSTS_AZURE_SERVICE?Need to set VSTS_AZURE_SERVICE}"

vsts-import $1 \
-g git@bitbucket.org:automationlogic/demoapplication.git  \
	-b ./build_def.json \
	-r ./releaseProcess-deploy.json \
	--releaseagent 'Hosted Linux Preview' \
	--releaseservice "connectedServiceNameARM=$VSTS_AZURE_SERVICE" \
	--releaseservice "ConnectedServiceName=$VSTS_AZURE_SERVICE" \
	-v DEV:applicationName=nodevmproject \
	-v DEV:resourceGroup=$2 \
	-v DEV:databaseName=nodevmdatabase 
