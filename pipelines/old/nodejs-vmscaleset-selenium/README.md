You will need to run this vm on a host with an xserver (either linux or install  [this](https://www.xquartz.org/) if on a mac) 
make sure Vagrant file is in your local path

```
vagrant up #takes a long time
vagrant ssh
/vagrant/basic_selenium.py
```
X-forwarding should be on by default in this vm. If its working you should see a chrome browser pop up and take you to the python website.

This demonstrates that selenium has installed on this vm.


now navigate to https://<yourVSTSenv>.visualstudio.com/<your project>/_machinegroup
![picture](1.png)
Create a new group called selenium, and then copy the bootstrap script onto your own vm.
![picture](2.png)