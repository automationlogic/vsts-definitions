timestamp="$(date +"%s")"

: "${VSTS_AZURE_SERVICE?Need to set VSTS_AZURE_SERVICE}"
: 	

vsts-import $1 \
-g https://bitbucket.org/automationlogic/demoapplication \
	-b ./buildProcess.json \
	-r ./releaseProcess-deploy.json \
	--buildagent 'Hosted Linux Preview' \
	--releaseagent 'Hosted Linux Preview' \
	--releaseagent 'Selenium=Default' \
	--releaseagent 'jmeter test=Hosted VS2017' \
    --releaseservice "connectedServiceNameARM=$VSTS_AZURE_SERVICE" \
	--releaseservice "ConnectedServiceName=$VSTS_AZURE_SERVICE" \
	--releaseservice "azureSubscriptionEndpoint=$VSTS_AZURE_SERVICE" \
	--buildservice "SonarQube=$VSTS_SONARQUBE_SERVICE" \
	-v applicationName=nvm$timestamp \
	-v location=westeurope \
	-v databaseAdminUsername=ALAdminDBUser \
	-v databaseAdminPassword='TestP4SSw0r1.3!' \
	-v databaseName=nvmdb$timestamp \
	-v Deploy:adminPassword='TestP4SSw0r1.3!' \
	-v Deploy:networkIpAddress="\"name\":\"ip01\",\"resourceGroup\":\"nvm$timestamp\",\"domainNameLabel\":\"nvm$timestamp\",\"publicIPAllocationMethod\":\"static\",\"newOrExistingOrNone\":\"new\"" \
	-v Deploy:resourceGroup=$2 \
	-v Deploy:applicationEnvironment=dev

echo REMEMBER: to change Build agents for some steps:
echo   - selenium on Default
echo   - jmeter on Hosted VS2017
#echo   - owasp to Default

