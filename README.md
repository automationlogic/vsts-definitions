# Azure Product CI definitions.

This repo stores build and release definitions for each platform type the product supports.

## Pipelines

Inside the pipelines folder are a collection of platform types for which we currently have pipelines defined.

These files are designed for use with our [vsts-import tool](https://bitbucket.org/automationlogic/vsts-rest)

## How do I get set up?

To use this tool, you must have:-

- Accounts:
	- a VSTS account
	- an azure subscription
	- a VSTS 'service endpoint' that links the two.
- Tools:
	- a build agent running locally or an agent in VSTS
	[The AL vsts-import tool](https://bitbucket.org/automationlogic/vsts-rest)

Use the instructions in the other repo to import your build and release definitions.

## To create a build / release definition

Make a new folder in the pipelines folder and store your build and release definitions there.

You can export existing definitions from the vsts UI and extract the relevant parts.

### Extract build definitions

1. Create a build definition that includes the build steps you want to include
2. Go back to the list of build definitions
3. Click the 'three dots' next to it and click export:

![Export build def](docs/images/export_build.png)

4. Save the file into this repo, in a new folder under ```/pipelines```


### Extract release definitions

1. Create a release definition that includes the build steps you want to include
2. Go back to the list of release definitions
3. Click the 'three dots' next to it and click export:

![Export release def](docs/images/export_release.png)


4. Save the file into this repo, in the same folder as your build definition.
