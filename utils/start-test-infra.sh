: "${VSTS_PAT?Need to set VSTS_PAT}"	
: "${VSTS_ACCOUNT?Need to set VSTS_ACCOUNT}"	

# az group create --name $1 --location westeurope

az container create \
	--name vsts-agent-$2 \
	--image microsoft/vsts-agent \
	--resource-group $1\
	--cpu 4 \
	--memory 8 \
	--environment-variables VSTS_ACCOUNT=$VSTS_ACCOUNT VSTS_TOKEN=$VSTS_PAT VSTS_AGENT=$2