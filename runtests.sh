set -e
set -x


pids=""

./pipelines/nodejs-vmscaleset/bitbucket-test.sh > nodejs-vmscaleset.log 2>&1 &
pids="$pids $!" 

./pipelines/nodejs-container/bitbucket-test.sh > nodejs-container.log 2>&1 &
pids="$pids $!" 

./pipelines/nodejs-webapp/bitbucket-test.sh > nodejs-webapp.log 2>&1 &
pids="$pids $!" 

wait $pids


